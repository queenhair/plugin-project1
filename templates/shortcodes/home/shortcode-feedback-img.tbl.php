<?php if(!empty($listItems[0])): ?>
    <section class="page-section">
        <div class="container">
            <h2 class="section-title"><span><?php echo !empty($atts['feedback_img_title']) ? $atts['feedback_img_title'] : '' ?></span></h2>
            <div class="top-products-carousel">
                <div class="owl-carousel" id="feedback-img-carousel">
                    <?php foreach($listItems as $item):?>
                        <div class="thumbnail no-border no-padding">
                            <div class="media">
                                    <div>
                                        <a class="media-link" data-gal="prettyPhoto" href="<?php echo !empty($item['img']) ? wp_get_attachment_url($item['img']) : ''?>">
                                            <img src="<?php echo !empty($item['img']) ? wp_get_attachment_url($item['img']) : ''?>" alt=""/>
                                        </a>
                                    </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>
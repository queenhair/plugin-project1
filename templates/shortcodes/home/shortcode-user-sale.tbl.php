<?php if(!empty($listItems[0])): ?>
<section class="page-section">
    <div class="container">
        <h2 class="section-title"><span><?php echo !empty($atts['user_sale_title']) ? $atts['user_sale_title'] : '' ?></span></h2>
        <div class="row">
        <?php foreach($listItems as $item):?>
            <?php if(count($listItems) > 2 ){ ?>
                <div class="col-md-3">
            <?php }else{ ?>
                 <div class="col-md-6">
            <?php } ?>
                    <div class="recent-post">
                    <div class="media">
                        <a class="pull-left media-link" href="#">
                            <img class="media-object" src="<?php echo !empty($item['img']) ? wp_get_attachment_url($item['img']) : ''?>" alt="">
                        </a>
                        <div class="media-body">
                            <p class="media-category"><a href="#"><?php echo !empty($item['name_sale']) ? $item['name_sale'] : ''?></a> </p>
                            <h4 class="media-heading">
                                <a href=" <?php echo !empty($item['link_insta']) ? $item['link_insta'] : ''?>" target="_blank" >
                                    <img src="<?php echo !empty($item['icon_insta']) ? wp_get_attachment_url($item['icon_insta']) : ''?>">
                                    <?php echo !empty($item['title_insta']) ? $item['title_insta'] : ''?>
                                </a>
                            </h4>
                            <h4 class="media-heading">
                                <a href=" <?php echo !empty($item['link_whatsapp']) ? $item['link_whatsapp'] : ''?>" target="_blank" >
                                    <img src="<?php echo !empty($item['icon_whatsapp']) ? wp_get_attachment_url($item['icon_whatsapp']) : ''?>">
                                    <?php echo !empty($item['title_whatsapp']) ? $item['title_whatsapp'] : ''?>
                                </a>
                            </h4>
                        </div>
                    </div>
                </div>
                 </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
<?php endif ?>
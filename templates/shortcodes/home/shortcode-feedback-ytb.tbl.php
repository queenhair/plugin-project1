<?php if(!empty($listItems[0])): ?>

    <section class="page-section">
        <div class="container">
            <h2 class="section-title"><span><?php echo !empty($atts['feedback_ytb_title']) ? $atts['feedback_ytb_title'] : '' ?></span></h2>
            <div class="partners-carousel">
                <div class="owl-carousel" id="feedback-ytb-carousel">
                    <?php foreach($listItems as $item):?>
                        <div>
                            <?php echo !empty($item['link_ytb']) ? $item['link_ytb'] : '#' ?>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </section>

<?php endif ?>
<?php if(!empty($atts['title'])):?>
<section class="page-section">
    <div class="container">
        <div class="message-box">
            <div class="message-box-inner">
                <h2><?php echo $atts['title'] ?></h2>
            </div>
        </div>
    </div>
</section>
<?php endif ?>
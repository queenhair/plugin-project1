<div class="widget widget-tag-cloud">
    <h4 class="widget-title"><?php echo !empty($instance['title']) ? esc_html($instance['title']) : '' ?></h4>
    <?php if(!empty($instance['linkMap'])):?>
        <iframe src="<?php echo $instance['linkMap'] ?>" style="width: 400px; height: 200px" frameborder="0" style="border:0" allowfullscreen></iframe>
    <?php endif ?>
</div>
<?php
namespace GMO\Shortcode\Home;

use GMO\Shortcode\AbstractShortcode;

class ShortCodeUserSale extends AbstractShortcode
{
    public function __construct($self = null) {
        $this->parent = $self;
        add_shortcode($this->get_name(), array($this, 'render'));
        vc_lean_map($this->get_name(), array($this, 'map'));
    }

    /**
     * Get shortcode name.
     *
     * @return string
     */
    public function get_name() {
        return 'user_sale_home';
    }

    /**
     * Shortcode handler.
     *
     * @param array $atts Shortcode attributes.
     *
     * @return string Shortcode output.
     */
    public function render($atts) {
        $atts = vc_map_get_attributes($this->get_name(), $atts);
        $atts = array_map('trim', $atts);
        $listItems = vc_param_group_parse_atts( $atts['items'] );
        ob_start();
        include $this->parent->locateTemplate('home/shortcode-user-sale.tbl.php');
        return ob_get_clean();
    }

    /**
     * Get shortcode settings.
     *
     * @return array
     *
     * @see vc_lean_map()
     */
    public function map() {
        $params = [
            [
                'type'       => 'textfield',
                'param_name' => 'user_sale_title',
                'heading'    => esc_html__('Tiêu đề', 'GMO'),
            ],
            [
                'type'       => 'param_group',
                'param_name' => 'items',
                'heading'    => esc_html__( 'List Item', 'GMO' ),
                'params'     => [
                    [
                        'type'       => 'attach_image',
                        'param_name' => 'img',
                        'heading'    => esc_html__('Ảnh', 'GMO'),
                    ],
                    [
                        'type'       => 'textfield',
                        'param_name' => 'name_sale',
                        'heading'    => esc_html__('Tên nhân viên', 'GMO'),
                    ],
                    [
                        'type'       => 'attach_image',
                        'param_name' => 'icon_whatsapp',
                        'heading'    => esc_html__('icon whatsapp', 'GMO'),
                    ],
                    [
                        'type'       => 'textfield',
                        'param_name' => 'title_whatsapp',
                        'heading'    => esc_html__('title whatsapp', 'GMO'),
                    ],
                    [
                        'type'       => 'textfield',
                        'param_name' => 'link_whatsapp',
                        'heading'    => esc_html__('link whatsapp', 'GMO'),
                    ],
                    [
                        'type'       => 'attach_image',
                        'param_name' => 'icon_insta',
                        'heading'    => esc_html__('icon insta', 'GMO'),
                    ],
                    [
                        'type'       => 'textfield',
                        'param_name' => 'title_insta',
                        'heading'    => esc_html__('title instagram', 'GMO'),
                    ],
                    [
                        'type'       => 'textfield',
                        'param_name' => 'link_insta',
                        'heading'    => esc_html__('link instagram', 'GMO'),
                    ]
                ]
            ]
        ];

        return array(
            'name'        => esc_html__('user sale-support', 'my-theme'),
            'description' => esc_html__('Trang chủ', 'GMO'),
            'category'    => $this->get_category(),
            'icon'        => $this->get_icon(),
            'params'      => $params
        );
    }
}

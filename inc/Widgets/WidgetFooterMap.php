<?php

namespace GMO\Widgets;

class WidgetFooterMap extends AbstractWidget
{
    function __construct()
    {
        // Instantiate the parent object
        parent::__construct('awesome_footer_map', 'Awesome Footer Map');
    }

    function widget($args, $instance)
    {
        include $this->locateTemplate('widgetFooter/WidgetsFooterMap.tpl.php');
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field($new_instance['title']);
        $instance['linkMap'] = sanitize_text_field($new_instance['linkMap']);

        return $instance;
    }

    function form($instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : '';
        $linkMap = !empty($instance['linkMap']) ? $instance['linkMap'] : '';

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'GMO'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('linkMap'); ?>"><?php _e('Link map:', 'GMO'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('linkMap'); ?>"
                   name="<?php echo $this->get_field_name('linkMap'); ?>" type="text"
                   value="<?php echo esc_attr($linkMap); ?>"/>
        </p>
        <?php
    }
}
